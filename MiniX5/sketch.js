let ellipseColor = 0
let rectColor = 0

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(1);
  ellipseColor = random(255);
  rectColor = random(255);
}

function draw() {
  background(255, 25, 50);
  stroke(ellipseColor);
  strokeWeight(1);
  noFill();
  for(var x = 0; x < width; x = x+10){
    if(frameCount % 20 == 10){
      circleColor = color(random(255), 200, random(200, 255));
      stroke(ellipseColor);
      ellipse(random(windowWidth), random(windowHeight), random(2,100,50));
    } else {
      rectColor = color(random(50, 255), random(100, 255));
      stroke(rectColor);
      ellipse(random(windowWidth), random(windowHeight), random(50, 100), random(50, 100));
    }
  }
}
  function mousePressed(){
  clear();
  background(255, 25, 50);
}
//rules:
//Change the size of the ellipses at random
//Ellipses appear in random places, and change their placement for each frame count
