# Aesthetic-programming-MiniX5

My Program: https://cecilievedsted.gitlab.io/aesthetic-programming-minix/MiniX5/

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX5/sketch.js

## My program

![ ](MiniX5/MiniX5.mov)

I have made a generative program, that shows a canvas with ellipses appearing randomly all over the canvas, in random sizes. When a new set of ellipses start to appear in random places, the "old" ellipses fade away in the background. When you click the mouse, you refresh the canvas. 

## Set of rules for my program

For this program, I made following rules:

1) Change the size of the ellipses at random.
2) Ellipses appear in random places, and change their placement for each frame count.


## Reflection

I chose to make a framerate function for this program, because I wanted a nice visualisation of the pattern, that I made in form of ellipses, that constantly changes over time. Furthermore i chose to incoporate "random function", that allows the shapes to create different colours over time. Which lets the computer generate some interesting and randoms things, that the programmer has no controll over. The for loop gives that effect, that the pattern changes every second because the ellipses changes their placement. 

The cool part about generativity is the fact, that it changes over time and a lot of cool and aesthetic visual programs have been made. Random functions and for loops are obvious functions in order to make these programs. When we think of random we often understand it as something that is out of our hands, something where the computer takes over. 

This week we were introduced to the term "pseudorandomness", from the chapter ‘Randomness’ in 10 PRINT CHR$(205.5+RND(1)); GOTO 10” by Nick Montford. This topic about randomness opens up to a discussion about; "Is random completely random?" How can something appear as random? Will there not always be an algorithmic rule behind a random function? 

With that said, this so-called random element has given great meaning to generative artwork, from  philosopher and artist, John Cage’s point of view,

“random elements remove individual bias from creation; they may be used to reach beyond the limitations of taste and bias through “chance operations.””(Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 126.)

By removing a small part of individualism from the generative program, the programmer can create something that even he can’t fully visualises until the code evolves self over time, and from there the programmer can chose to tinker more to the coding, or she can let it be for others to enjoy.


## References

- Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 126. 
- Soon, Winnie & Cox, Geoff. Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press, 2020. p. 123-140. 
- Shiffman, Daniel - The coding train. Uploaded 11 sep. 2015. "Nested Loops - p5.js Tutorial". https://www.youtube.com/watch?v=1c1_TMdf8b8&t=160s 

