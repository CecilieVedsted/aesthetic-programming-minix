let slider;
let emoji1,emoji2,emoji3,emoji4;

// pre-load function of the four emoji moods
function preload() {
  emoji1 = loadImage('emoji1.png');
  emoji2 = loadImage('emoji2.png');
  emoji3 = loadImage('emoji3.png');
  emoji4 = loadImage('emoji4.png');
}

//values for where the slider is placed
function setup() {
  createCanvas(800, windowHeight);
  slider = createSlider(0, 15, 0);
  slider.position(50, 700);
  slider.size(660);
  }

//values for changeing emoji mood
  function draw() {
  let val = slider.value();

if(val==0){
  background(emoji1)}

  else if(val==5){
  background(emoji2)}

else if(val==10){
background(emoji3)}

else if(val==15){
background(emoji4)
}

//"I am feeling" text
fill(0,255);
textSize(40);
textAlign(CENTER);
text('I am feeling:', width/2,150);
textSize(12)

//Four moods in text at the buttom on the slider
text('Grateful', 80, 690);
text('Sad', 280, 690);
text('Happy', 480, 690);
text('Cold', 695, 690);
}
