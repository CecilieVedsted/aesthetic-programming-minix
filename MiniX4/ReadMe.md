# Aesthetic-programming-MiniX4

My Program: https://CecilieVedsted.gitlab.io/aesthetic-programming-minix/MiniX4/

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX4/sketch.js

## My program

![ ](MiniX4/MiniX4.mov)

I have made at slider that can express diffent moods in an emoji. You have the opportunity to choose from four different moods. Therefore I called my work "I am feeling:" I got the inspiration of emoji from our previously MiniX.

## Reflection

I think the whole discussion about data politics are important. To question how our personal data is captured, quantified, archived, and used, and for what purpose? What are the implications, and who has the right to access the captured data, and derive profit from it? My thoughts/goals in the beginning was to make a program, that could respond to the users voice and from that information create an emoji, that responded to the data. But I could not make it work. Therefore I decided to make a slider, that could express diffent moods. So I started brainstorming on different ideas for the slider. When I think about a slider, I think about instagram. On Instagram you have the opportunity to share stuff from your own life with friends, and they everyone who are following you have the opportunity to react on it with emojies. Which I think is a nice way to communicate in a visuel way. This data can then be further utilized to analyze customers’ profiles and user behaviors. 

The use of a single Like button provides a good example of how our feelings are captured and also when you react on something by choosing a mood. It provides a kind of feedback. On Facebook there are six emoticons including “Like,” “Love,” “Haha,” “Wow,” “Sad” and “Angry,” mark our standardized experience of work and play more precisely. All clicks are “categorized” into emotional metrics, displayed publicly on the web, and used for algorithmic calculation to prioritize feeds to users. It is fairly clear how the clicks serve the interests of platform owners foremost, and, as if to prove the point, Facebook, and Instagram have tested the idea of hiding the metrics on posts in order to shift attention to what they prefer to call “connecting people” — as if to prove their interests to be altruistic.

I also think Zuboffs take on the whole data capture is very interesting and relevant in todays world. She asks the question: why "surveillance?" Because it is necessarily operations that are fabricated in a way that they are undetectable, incomprehensible and hidden in rhetoric, with the aim of diverting our attention and creating ambiguity and simply cheating us all.

## My syntax

In this assignment I have used syntaxes such as preload, if/else statements and slider. I had some inspiration from the required reading in the textbook. I decided to have four different expression (emojies). I spend a lot of time playing around and changeing the values in my program, so it would work and the correct emoji would appear when moving the slider. This was my first time using uploading pictures to my program, so I was excited when I made it work. It would still have been cool, of there was some kind of either voice/video capturing, but that must be another time. 

## References
When I think about a slider, my thoughts are directed to instagram. Where you have the opportunity to share stuff from your own life with friends and other people who are following you, and they have the opportunity to react on your stories and messages with emojies. Which I think is a nice way to communicate in a visuel way instead of having to text back and fourth. 


- Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
- Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw


