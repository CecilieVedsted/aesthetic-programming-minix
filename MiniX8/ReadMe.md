# Aesthetic-programming-MiniX8

Our program: https://cecilievedsted.gitlab.io/aesthetic-programming-minix/MiniX8

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX8/sketch.js

Json file: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX8/members.json

## My program 

![ ](MiniX8/MiniX8_.mov) 


## Short description

Our project is called “The Sim like introduction” and consists of a program where you have the opportunity to click four different buttons, that represents each of the group members and furthermore information about the members in study group 10. The buttons contain the following information; name, age, gender and interests. When a button is clicked a certain background color will appear, a colour that specific person has chosen.


## How does it work?

This is the part where the JSON files comes in. We have a single JSON file that contains information about the four group members. The four members are found as four objects in the same array, making it easier to call the needed information into the sketch file. 


In the sketch/javascript file, we first make sure to load the json files in the preload() function.
We then select the shown data based on conditions in an if else () statement.
 
Furthermore we have used a “hack” learned by Mads (instruktor) where we put a function in a function. This was a way to differentiate the button from each other in the loadData function. We asked him, because for some reason, the function would recognize the name of the button as a condition. Mads showed us how to implement a function in our mousepressed function, that would send a word to loaddata. This made the function accept conditions. 

 The rest of the code is basically a little bit of flavor to make it look better.


## Vocable code - analysis:

In the courses we have talked a lot about how it has been established, that the computer is not intelligent and that it is our task ( as humans)  to give the computer instructions. The same concept is written in the text “Vocable Code” by Geoff Cox and Alex McLean in the paragraph “Notation”, where the authors argue, that the meaning of the program and how it’s supposed to be understood by others is being ignored by the computer. The computer may extract the words that are being written in the code, but the context of the words and the program is only for humans to understand.

Made by Mathias Højgård and Cecilie Vedsted 

## Reference 

- Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186
- Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.
- Allison Parrish, “Text and Type” (2019), https://creative-coding.decontextualize.com/text-and-type/.
- Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r
- Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r
