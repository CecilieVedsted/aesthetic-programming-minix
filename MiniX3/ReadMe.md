# Aesthetic-programming-MiniX3

My Program: https://cecilievedsted.gitlab.io/aesthetic-programming-minix//MiniX3/

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX3/sketch.js

## My program 

![ ](MiniX3/MiniX3.mov) 

My first idea was to make a simple and minimalistic throbber, like the one we worked with in class. Therefore I started making simple changes to the program, by changing the background color and the number of ellipses displayed in the canvas. Thereafter I tried looking up different throbbers and loading elements to get inspiration. I used a formula from p5js and experimented with different shapes and rotations. I simply used the shape ellipse and circle and adjusted some of the measurements for better aesthetics. 


## Reflection 

My thought with this throbber was to make some kind of watch. Since I associate a throbber with time. Often it is waiting time, when a program or movie is loading. But my throbber can also symbolize a form of loop. Something that runs all the time in a circle, and that keeps the rhythm at all time. That does not change, does not go faster, does not go slower and does not stop. 

Contrary to the concept of time in the clock is the time found in coding. It does not work in the same way, as we know it from human life. In real life, time is something that continuously works linearly and uncontrollably like I wrote earliere. In coding, the programmer can play god by manipulating time in the desired way. Time can be stopped / started, reversed / looped, lowered / accelerated. Time is controllable and it can be skipped and looped to infinity. It is a very paradoxical concept consisting of many layers with multiple timelines. This gives the programmer a power that does not exist in the real world.

Which soon and cox also talks about in their book.

"We might go as far as to say that programming allows for a time-critical understanding of how technologies play a crucial role in our experience of time..." (p. 93) 

Throughout this miniX, the biggest challenge has been making a rotation in my throbber. I have had a lot of help by looking in the provided required reading ("Infinite loops", p5.js) that demonstrated how to make it work. The background is drawn with the RGB.


## References

Soon, Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
