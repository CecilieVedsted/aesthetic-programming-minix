function setup() {
  createCanvas(600, 550);
}

function draw() {
  background(255,200,300);
  fill(0,50,300,200);
  stroke(125,60,300);
  //to get the throbber in the center
  translate(300,280,60,50);
  noStroke();
//make it rotate and have 10 "ellipses"
  rotate(map(frameCount%360,0,360,0,2*PI));
  for(var i=0; i<10;i++) {
    ellipse(200,40,100,20,80,65,40,80);
    rotate(PI/5);}
    //circle in the middel
  circle (5,5,30)
  fill(10,200,230);
}
