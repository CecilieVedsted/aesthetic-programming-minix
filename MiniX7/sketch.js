
let paddleSize = { //size of "catcher"
  w:120,
  h:15
};

let raindropimg;
let cloudimg;
let min_raindrop = 3; //minimum numbers of raindrops on the screen
let raindrop  = []; //array
let paddlePosX;
let mini_width;
let score =0, lose = 0;
let cloudX =150;
let cloudY = 200;




function preload(){
  raindropimg = loadImage('raindrop.png');
  cloudimg =  loadImage('cloud.png');
}


function setup(){
  createCanvas(windowWidth,windowHeight);
  paddlePosX = width/2;
  mini_width = width;

}


function draw(){
  background("#88CEFA");
  image(cloudimg, windowWidth-325,windowHeight-560); //Size of cloud
  displayScore();
  checkRaindropNum();
  showRaindrop();
  noStroke();
  fill("#90EE900");
  rect(paddlePosX, height-20, paddleSize.w, paddleSize.h);
  checkCaught(); //Checks if the raindrops have been caught
  checkResult();//Checks the result
}

function checkRaindropNum(){
  if(raindrop.length < min_raindrop){
    raindrop.push(new Raindrop());
  }
}

function showRaindrop(){
  for(let i=0; i < raindrop.length; i++){
    raindrop[i].move();
    raindrop[i].show();
  }
}

function checkCaught(){
  for (let i = 0; i < raindrop.length; i++) {
  let d = int(
    dist(paddlePosX + paddleSize.w/2, height-paddleSize.h/2,
      raindrop[i].pos.x, raindrop[i].pos.y)
    );
  if (d < paddleSize.w/4) {
    score++;
    raindrop.splice(i,1);
  }else if (raindrop[i].pos.y > height) {
    lose++;
  raindrop.splice(i,1);

    }
  }
}


function displayScore() {
    fill("#FA800");
    textSize(15);
    text('You have caught: '+ score + " raindrop(s)", 60, height/1.15);
    text('You have lost: '+ lose + " raindrop(s)", 60, height/1.15+20);
    fill("#FA800");
    text('PRESS THE ARROW LEFT AND RIGTH IN ORDER TO CATCH THE RAINDROPS',
    15, height/1.4+60);
}


function checkResult() {
  if (lose > score && lose > 4) {
    fill("#FA800");
    textSize(50);
    text("GAME OVER", width/3, height/1.4);
    noLoop();
  }
}


function keyPressed() {

  if (keyCode === LEFT_ARROW) {
    paddlePosX-=100;//How fast you can move the "catcher"
  } else if (keyCode === RIGHT_ARROW) {
   paddlePosX+=100;
  }
  //Reset if the paddle moves out of range
  if (paddlePosX > mini_width) {
   paddlePosX = mini_width;
 } else if (paddlePosX < 0 - paddleSize.w/4) {
 paddlePosX = 0;
 }
}
