// Class name
class Raindrop{
  constructor(){
    this.speed = floor(random(1,4));
    this.size = floor(random(35, 60));
    this.pos = new createVector(random(windowWidth), 0);
  }

//Inspiration from the Tofu-game
  move(){
    this.pos.y += this.speed; //this.pos.x = this.pos.x - this.speed
  }

  show(){
    imageMode(CENTER);
    image(raindropimg, this.pos.x, this.pos.y, this.size, this.size);
  }

  }
