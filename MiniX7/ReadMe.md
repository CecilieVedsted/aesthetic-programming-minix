# Aesthetic-programming-MiniX7

My program: https://cecilievedsted.gitlab.io/aesthetic-programming-minix/MiniX7/

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX7/sketch.js

## My program 

![ ](MiniX7/My_game.png) 

In my MiniX7 I have made a game where you have to try catch raindrops with a paddle, that is placed at the bottom of the screen. In order to move the paddle and furthermore catch the raindrops you use the left and right arrows on your keyboard. You actually can’t win the game, but you lose the game when the sum of lost suns is 4 more than the suns you have caught. The cloud does not have a purpose for the game, I just put it on the canvas to give the user a better feeling of the game with raindrops and a cloud. 

I have decided from the beginning, that I would use the Tofu game as inspiration and futhermore I found another game on google, that I had some inspiration from. The objects in my program are the raindrops, that falls down from the sky as rain.


## Reflection 

When I made my MiniX7, I knew from the beginning, that I would not able to produce a game from scratch. Therefore I used the tofu example and focused on understanding the concept of OOP. For a start I changed the movement of the tofu form horizontally to vertically and made the obect a raindrop instead of the tofu's. And afterwards I added the paddel as a form of "catcher", cause I thought that would be quite nice. 

Soon and Cox describes object abstractions:

“Object abstraction in computing is about representation. Certain attributes and relations are abstracted from the real world, whilst simultaneously leaving details and contexts out.” (Soon and Cox 2020, p. 146).

From this I think that the use of a raindrop as my objects causes that, I have avoided any too complex abstractions, so that most people will understand the purpose of the game from the very beginning. The created objects are usually very subjective, because the programmer has selected to highlight some things while others are ignored.


## References: 

- Tofu Game by Winnie 
- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- https://editor.p5js.org/ehersh/sketches/Hk52gNXR7 (game from google)
