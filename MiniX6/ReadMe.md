# Aesthetic-programming-MiniX6

My Program: https://cecilievedsted.gitlab.io/aesthetic-programming-minix/MiniX6/

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX6/sketch.js 

## My program

![ ](MiniX6/MiniX1.png)
![ ](MiniX6/MiniX6.png)

I decided to work on my MiniX1. This was my very first time trying programming, and I felt it was that project that could use the most improvement.  I wanted to implement some syntax I have learnt over the past classes – like the random-function. Another focued on this revisit was, that I wanted to make the program more aesthetically looking. 

## Reflection 

In order to make my work more aesthetically pleasing to look at I implemented some more ellipses in different colours. I felt that the person in my miniX was very simpel. I did not have a plan with this MiniX from the beginning, I just experimented with different syntaxes and it endded up like a person made by different shapes. 

In general the digital culture has become an important part of our everyday life. We see it everywhere. Therefore aesthetic programming plays a bigger and bigger role in the digital art. Programming makes experiences more mobile, because you can take it with you everywhere and watch it on your mobile, computer etc.

During this week I found it to by rather helpful to reflect on the semester and look what I have learned so far. When coming into this course I wanted to get a better insight in code as practice and understand what is happening behind the interface. I have found out during the classes, that I enjoy the conceptual part more, than the coding part, but I still find it cool, that I improve my own coding skills from week to week. 

I have found out, that programming in generel is not only used to make profit, by inventing an app or a website, but also a way to express our self aesthetically though code, and aesthetic programming has given us the opportunity to do so while getting a clearer understanding on the computational culture. 

## Syntax

In this MiniX I implemented the random function, in order to make the program look my aesthetic and furthermore experimented with different colours to make the program look more aesthetically nice.  

## Inspiration

https://editor.p5js.org/kellylougheed/sketches/K6PoaHCNv



