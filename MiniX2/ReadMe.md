# Aesthetic-programming-MiniX2

My Program: https://cecilievedsted.gitlab.io/aesthetic-programming-minix/MiniX2/

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX2/sketch.js

## My program 

![ ](MiniX2/screenshot_1.png) 

![ ](MiniX2/screenshot.png) 

## Reflection 

I have made a smiley face, that can start screaming when you move the mouse. The majority of the smiley have I chosen to be red, so it does not offend anyone in terms of their nationalities and skin color. Therefore, I chose red, as a neutral color. Some of my thoughts are connected to Roel Roscam Abbing, Peggy Pierrot and Femke Sneltings text "Modifying the Universal" (2018), and specifically the paragraph "Because one face does not fit all" (p.38-41) Furthermore the emoji can smile and scream. I found it difficult to make an emoji, cause there already exsist so many different kinds exist. But i choose these two moods "smiling" and "screaming/surprised." Because it is those two expression, that I use the most over message. I have worked with geometric shapes (ellipses,rectangles, lines and triangles) to create my emojis. 

I have experimented with ellipses and other shapes in this task in the form of both the face, mouth and two eyes and colors. I have had a lot of help by looking in the provided required reading ("variables", p5.js) that demonstrated how to use the mouse X, and mouse Y and played around with it. I still find it difficult to get all the functions to work. Which can be very frustrating. 

## My syntax

I have used conditional structures (if-statements (see line 27-39) with mouseX and mouseY to make my emoji smile and scream, despending on where the mouse is on the canvas. In conclusion I have mainly used geometric structures to make this program in different shapes, colors and sizes.

In this program I chose to introduce conditional structures (if-statements). In the beginning it was quite difficult to figure out how to organize the statements, and I still don't understand the structure completely. Therefore i went with a pretty basic code, cause I am still working on my coding skiils. In the end with some help from The Coding Train on YouTube and Google I made it work. When I start a new programming project, I always get reminded of the continuing process of trial and errors. It can be very frustrating at times when things don't run as planned but, in the end it is especially rewarding when you see the final result. 

## Emoji 
One important aspect of emojis and emoticons is that they should be understandable and relatable to the majority as well as being inclusive to diverse people and cultures. Emojis is supposed to be funny and creative as well as being an abstraction of people’s emotions. It is a difficult challenge to create emojis that embrace everybody’s personalities and cultures without them being oversimplified and discriminating to some. This inequality in emojis is one of the reasons why I tried avoiding recognizable human features in my emoji and therefore made it red and very "normal" looking . But I still chose to use very traditional shapes (ellipses) which is very universal to the emojis that already exists.

## References 

- Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
- Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda - Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016 [Video, 1 hr 15 mins].
- Daniel Shiffman, Code! Programming with p5.js, The Coding Train https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2
