function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(220,200,500);
  //colour of the face
  fill(300,0,0);
  //size of the face 
  ellipse(200,200,200,200);

  //white around the pupil
  fill(355,355,255);

  //right eye
  ellipse(240,140,20,20);
  //left eye
  ellipse(160,140,20,20);
  fill(0,0,0);
  //pupil right 
  ellipse(240,140,10,10);
  //pupil left
  ellipse(160,140,10,10);
    arc(300, 150, 25, PI);
  arc(width/2, height/3 + 50, 70, 30, 0, PI, CHORD);

    if (mouseX > 100 && mouseY > 100) {
 fill(150,5,5);
  ellipse(200,200,150,80);
 fill(240,140,200);
  ellipse(200,210,120,60);
 fill(0);
  line(600,510,180);


  } else {
    fill(0);
    line(250,250,275);}
}
