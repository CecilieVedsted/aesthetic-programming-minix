# Aesthetic-programming-MiniX1

My Program: https://CecilieVedsted.gitlab.io/aesthetic-programming-minix/MiniX1/

Code level: https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/blob/master/MiniX1/sketch.js 

## My program 

![ ](MiniX1/MiniX1.png)

### What have I produced?

This was my first time trying to code and it was an experience with mixed feelings. I have produced a little person and experimented with rectangles and ellipses in different colors. I had a lot of help from the p5.js references. I just tried a lot of different things, to see what effect it would have, when I changed different arguments. I just played around with numbers in p5.js, the order of the code and so on. I think it is difficult to learn this new code language, because it is not only letters and numbers, but also signs (",;,{}) and if just one number, letter or sign is misplaced it will not work at all. So you really get to test your patience.

## Reflection 

### How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?

I have mainly focused on ellipse, rect and the color functions R,G,B + alpha. It was very fun and sometimes frustrating to explore and work with the references. I find it very difficult to "spot" my own mistaskes, when my code won't be executed. In order to make my own code i watched a lot of "Coding Train" videoes by Daniel Shiffman and Lauren Mccarthy on youtube, and that helped a lot!!It is also very fun to code (when it works (!!!), and see how my own code gets executed to something "real." 

Coding is very rewarding in the sense that you can to play around with different numbers and how you understand the meaning of the individual factors of a single code by changing out numbers or the order of the variables and functions. I can look at a piece of code and get kind a curious as to how that piece is coded. Furthermore reading code is a lot less daunting now, there is a lot I do not understand (even in my own project) but having a little understanding for the syntax and knowing that maybe you can figure out the meaning of a code by either playing with it or simply look it up is reassuring. 

### How is the coding process different from, or similar to, reading and writing text?

For me coding and programming in general are recepies or instructions, that I need to follow. I've always thought of coding it in terms of websites. But never as art or a way to express myself, which I think is really cool. This weeks assigned readings opened my eyes to the possibilites there is in coding and showed how instrumental it is in our everyday lives by explaining programming as a literacy just like reading or writing.

## My syntax 

I have used two functions: function setup and function draw. There are also used different shape parameters such as: rect and ellipse to create the body and head. The body, head, eyes are filled with different colors through the command fill();, and some of the colors are overlapping because of the alpha feature in the fill();. The different parts of the person is placed in the center of the canvas by using the rectMode and ellipseMode with the argument 
CENTER.

## Inspiration 

Coding is a way to communicate with a computer in a common language - especially in a high-level programming language like javascript. The computer’s mother tongue is binary code, and ours is one of many human languages. By using code, human and computer can communicate directly.
As coding becomes more and more attainable to more and more people, people have started to consider how to include everyone. Here, programming is viewed as something similar in importance to reading, writing and mathematics, in that it has started to take up more space in our world - this view is exemplified in the term “literacy”, a connection to the aforementioned reading and writing.

Anette Vee argues for 4 dominant arguments for mass programming which are:
Individual empowerment,
Learning new ways to think,
Citizenship and collective progress,
Employability and economic concerns (Vee, p. 2017 76).

And we believe that these are some important reasons to learn programming and to mass programming. In our field of study, programming is a big part of our future. We will most likely end up working with programming or with programmers, therefore it can be seen as an important skill when it comes to employability. It also provides new ways of thinking and learning.

## References
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
- Video: Daniel Shiffman, Code! Programming with p5.js, The Coding Train. Available at https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2
- (Vee, p. 2017 76)
